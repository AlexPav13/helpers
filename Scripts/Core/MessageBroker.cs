using System;
using System.Collections.Generic;

namespace AlexHelpers
{
    public sealed class MessageBroker
    {
        public readonly static MessageBroker Default = new MessageBroker();

        private readonly Dictionary<Type, List<Registration>> registry = new Dictionary<Type, List<Registration>>();

        public void Publish<T>(T message)
        {
            Type key = typeof(T);
            if (registry.ContainsKey(key))
            {
                var registrationsCopy = new List<Registration>(registry[key]); // copy for thread safety
                foreach (var registration in registrationsCopy)
                    registration.observer(message);
            }
        }

        public void AddObserver<T>(Action<T> observer)
        {
            lock (registry)
            {
                Type key = typeof(T);
                var registrations = registry.ContainsKey(key) ? registry[key] : new List<Registration>();
                registrations.Add(new Registration { reference = observer, observer = obj => observer((T)obj) });
                registry[key] = registrations;
            }
        }

        public void RemoveObserver<T>(Action<T> observer)
        {
            lock (registry)
            {
                Type key = typeof(T);
                if (registry.ContainsKey(key))
                {
                    var registrations = registry[key];
                    registrations.RemoveAll(reg => reg.reference == (object)observer);
                    registry[key] = registrations;
                }
            }
        }

        private class Registration
        {
            public object reference;
            public Action<object> observer;
        }
    }
}