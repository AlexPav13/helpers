﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace AlexHelpers
{

    public interface IAnimator
    {
        void StartAnimation(SettingShowAnimation showAnimation, Action callback);

    }

    public enum SettingShowAnimation {ShowScene, HideScene, NotPlayning }
}