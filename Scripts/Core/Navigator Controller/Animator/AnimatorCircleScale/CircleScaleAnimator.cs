﻿using AlexHelpers;
using DG.Tweening;
using System;
using UnityEngine;

public class CircleScaleAnimator : MonoBehaviour, IAnimator
{
    public RectTransform imageCircle;
    public float duration;

    public void Awake()
    {
        if (NavigatorController.Default.ShowAnimationTransitoin)
        {
            imageCircle.gameObject.SetActive(true);
            StartAnimation(SettingShowAnimation.ShowScene, null);
        }
        else
        {
            imageCircle.gameObject.SetActive(false);
        }
    }

    public void StartAnimation(SettingShowAnimation showAnimation, Action callback)
    {
        if (showAnimation != SettingShowAnimation.NotPlayning)
        {
            if (showAnimation == SettingShowAnimation.ShowScene)
            {
                imageCircle.localScale = new Vector3(Screen.height * 3, Screen.height * 3, 0);
            }
            else if (showAnimation == SettingShowAnimation.HideScene)
            {
                imageCircle.localScale = new Vector3(1, 1, 0);
            }
            imageCircle.gameObject.SetActive(true);
            float scalaleImage = showAnimation == SettingShowAnimation.ShowScene ? 1 : Screen.height * 3;
            AnimationImage(scalaleImage, callback);
        }
        else {
            callback();
        }
    }

    void AnimationImage(float scalaleImage, Action callback)
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(imageCircle.DOScale(scalaleImage, duration)).
            AppendInterval(0.1f).AppendCallback(() => { if (callback != null)callback(); })
            .SetEase(Ease.InSine);
    }
}
