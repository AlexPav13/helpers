﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlexHelpers
{

    public class NavigatorController
    {
        public static readonly NavigatorController Default = new NavigatorController();
        public bool ShowAnimationTransitoin = false;

        public NavigatorController()
        {
            MessageBroker.Default.AddObserver<Transition>(Transition);
        }

        public void Transition(Transition transition)
        {
            AsyncOperation async = null;
            if (transition.asyncLoadScene)
            {
                async = SceneManager.LoadSceneAsync(transition.sceneName);
                async.allowSceneActivation = false;
            }

            if (transition.transitionType == TransitionType.Restart)
            {
                ShowAnimationTransitoin = false;
                if (transition.asyncLoadScene)
                    transition.animator.StartAnimation(SettingShowAnimation.NotPlayning, () => async.allowSceneActivation = true);
                else
                    transition.animator.StartAnimation(SettingShowAnimation.NotPlayning, () => SceneManager.LoadScene(transition.sceneName));
            }
            else
            {
                ShowAnimationTransitoin = true;
                if (transition.asyncLoadScene)
                    transition.animator.StartAnimation(SettingShowAnimation.HideScene, () => async.allowSceneActivation = true);
                else
                    transition.animator.StartAnimation(SettingShowAnimation.HideScene, () => SceneManager.LoadScene(transition.sceneName));
            }
        }



    }

    public class Transition
    {
        public string sceneName;
        public IAnimator animator;
        public TransitionType transitionType;
        public bool asyncLoadScene;
        public Transition(string sceneName, IAnimator animator, TransitionType transitionType, bool asyncLoadScene)
        {
            this.sceneName = sceneName;
            this.animator = animator;
            this.transitionType = transitionType;
            this.asyncLoadScene = asyncLoadScene;
        }
    }

    public enum TransitionType { Forward, Back, Restart }
}